// Place your application-specific JavaScript functions and classes here
// This file is automatically included by javascript_include_tag :defaults

$(document).ready( function() {
	/* $("#circle-it-container").fadeTo("fast", 0).css("background", "url(/images/circle_it_50.png) no-repeat left top"); */
	$("#circle-it-container").fadeTo("fast", 0).css("background", "transparent");   /*.unbind("mousedown").unbind("mousemove").unbind("mouseover").unbind("mouseout").unbind("mouseenter").unbind("mouseleave").unbind("mouseup"); */

	initCircleItBinding();

	var myToggle = 1;

	var circleItAdjustRightPx  = -15;
	var circleItAdjustDownPx   = 5;
	var circleItAdjustWidthPx  = 30;
	var circleItAdjustHeightPx = -5;

	function initCircleItBinding() {
		// place in function so can rebind
		// alert("Binding ...");
		$(".circleable").bind("mousemove", ( function () {
			var containerWidth = $(this).css("width");
			var containerHeight = $(this).css("height");
			// alert("Circle container width=" + circleContainerWidth + "     circleContainerHeight = " + circleContainerHeight);
			$("#circle-it-image").css("height", ((parseInt(containerHeight, 10) + 20 + circleItAdjustHeightPx)) + "px");
			$("#circle-it-image").css("width",  ((parseInt(containerWidth, 10) + 30 + circleItAdjustWidthPx)) + "px");
			var offset = $(this).offset();
			$("#circle-it-container").offset({ top: offset.top - 10 + circleItAdjustDownPx, left: offset.left - 10 + circleItAdjustRightPx}).css("z-index", "500");
			$("#circle-it-container").fadeTo(0, 1.0);
			// alert("Cirle It!  offset.left=" + offset.left + "   offset.top=" + offset.top);
		}));   /*.mouseout( function () {
				// alert("Circle It mouse out");
				$("#circle-it-container").fadeTo("slow", 0);
				}); */
	}


	$("#circle-it-container").mouseleave( function () {
		$(".circleable").unbind("mousemove");
		// $("#circle-it-container").fadeTo(0, 0.1);
		/* var currentOffset = $(this).offset(); */
		// $(this).offset({top: 400, left: 0}).css("width", "1px").css("height", "1px").css("border-width", "1px");
		myToggle = myToggle * -1;
		// if(myToggle < 0) { $("#circle-it-image").css("width", "100px").css("height", "100px"); } else { $("#circle-it-image").css("width", "50px").css("height", "50px"); }
		$(this).css("z-index", "-1");
 		setTimeout(function() {
			initCircleItBinding();
		}, 10);
	});


	$("li.complete-goal").parents("tr").css("background-color", "#eeeeee");


	$("li.incomplete-goal").click( function() {
		var data_id = $(this).parents("tr").attr("data-id");
		alert("data id = " + data_id);
		$.ajax({
			url: "goals/close",
			data: { id : data_id },
			success: alert("Done")
		});
	});
});

