require "rubygems"
require "sanitize"



class GoalsController < ApplicationController
  before_filter :authenticate_user!, :except => [:index]


  # GET /goals
  # GET /goals.xml
  def index
    if user_signed_in?
      logger.debug("User signed in")
      @userid = current_user.id
      @goals = Goal.all(:userid =>@userid, :order => "is_complete ASC, priority ASC")
    else
      logger.debug("User not signed in")
      # to do: maybe have a sample user with fake goals show up on home page
    end


    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @goals }
    end
  end

  # GET /goals/1
  # GET /goals/1.xml
  def show
    @goal = Goal.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @goal }
    end
  end

  # GET /goals/new
  # GET /goals/new.xml
  def new
    @goal = Goal.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @goal }
    end
  end

  # GET /goals/1/edit
  def edit
    @goal = Goal.find(params[:id])
  end

  # POST /goals
  # POST /goals.xml
  def create
    params[:goal][:name] = Sanitize.clean(params[:goal][:name])
    @goal = Goal.new(params[:goal])
    @userid = current_user.id.to_i
    logger.debug("##### GoalsController#create - current_user userid=" + @userid.to_s)
    @goal.userid = @userid

    respond_to do |format|
      if @goal.save
        format.html { redirect_to(@goal, :notice => 'Goal was successfully created.') }
        format.xml  { render :xml => @goal, :status => :created, :location => @goal }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @goal.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /goals/1
  # PUT /goals/1.xml
  def update
    @goal = Goal.find(params[:id])
    is_complete_flag = params[:complete_flag]
    logger.debug("is_complete_flag = #{is_complete_flag} ")
    if is_complete_flag && is_complete_flag.upcase == "YES"
      @goal.is_complete = true
    else
      @goal.is_complete = false
    end

    respond_to do |format|
      if @goal.update_attributes(params[:goal])
        format.html { redirect_to(@goal, :notice => 'Goal was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @goal.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /goals/1
  # DELETE /goals/1.xml
  def destroy
    @goal = Goal.find(params[:id])
    @goal.destroy

    respond_to do |format|
      format.html { redirect_to(goals_url) }
      format.xml  { head :ok }
    end
  end


  # custom action to finish/close a goal
  def close
    @goal = Goal.find(params[:id])
    logger.debug("##### GoalsController#close - @goal.id = #{@goal.id} ")
    @goal.is_complete = true
    @goal.save(:validate => false)
    redirect_to :action => :index
  end

end


