class TasksController < ApplicationController
  before_filter :authenticate_user!


  # GET /tasks
  # GET /tasks.xml
  def index
    if user_signed_in?
      userid = current_user.id
      logger.debug("##### TasksController#index - userid=#{userid}")
      goals = Goal.all(:userid.in => [userid])
      goal_id_array = goals.map{|g| g.id}
      logger.debug("##### TasksController#index - goal_id_array = #{goal_id_array.inspect}")
      @tasks = Task.all(:goal_id.in => goal_id_array)
    end

    respond_to do |format|
      format.html # index.html.erb
      format.xml  { render :xml => @tasks }
    end
  end

  # GET /tasks/1
  # GET /tasks/1.xml
  def show
    @task = Task.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.xml  { render :xml => @task }
    end
  end

  # GET /tasks/new
  # GET /tasks/new.xml
  def new
    @task = Task.new

    respond_to do |format|
      format.html # new.html.erb
      format.xml  { render :xml => @task }
    end
  end

  # GET /tasks/1/edit
  def edit
    @task = Task.find(params[:id])
  end

  # POST /tasks
  # POST /tasks.xml
  def create
    @task = nil

    if user_signed_in?
      @task = Task.new(params[:task])
    end

    respond_to do |format|
      if @task.save
        format.html { redirect_to(@task, :notice => 'Task was successfully created.') }
        format.xml  { render :xml => @task, :status => :created, :location => @task }
      else
        format.html { render :action => "new" }
        format.xml  { render :xml => @task.errors, :status => :unprocessable_entity }
      end
    end
  end

  # PUT /tasks/1
  # PUT /tasks/1.xml
  def update
    @task = Task.find(params[:id])

    respond_to do |format|
      if @task.update_attributes(params[:task])
        format.html { redirect_to(@task, :notice => 'Task was successfully updated.') }
        format.xml  { head :ok }
      else
        format.html { render :action => "edit" }
        format.xml  { render :xml => @task.errors, :status => :unprocessable_entity }
      end
    end
  end

  # DELETE /tasks/1
  # DELETE /tasks/1.xml
  def destroy
    @task = Task.find(params[:id])
    @task.destroy

    respond_to do |format|
      format.html { redirect_to(tasks_url) }
      format.xml  { head :ok }
    end
  end


  ##### Added methods #####
  def tasks_for_goal
    goal_id = params[:id]
    logger.debug("##### TasksController#tasks_for_goal - goal_id = #{goal_id}")
    @goal = Goal.all(:id => goal_id)[0]

    if user_signed_in?
      valid_goals = Goal.all(:userid => current_user.id)
      valid_goal_ids = valid_goals.map{|g| g.id.to_s}
      logger.debug("##### TasksController#tasks_for_goal - valid_goal_ids= #{valid_goal_ids.inspect}")

      if valid_goal_ids.include?(goal_id.to_s)
        # ensure task param id belongs to this user
        @tasks = Task.all(:goal_id => goal_id)
      else
        logger.debug "Security Error - requested access to task that does not belong to user"
        raise "Security Error - requested access to task that does not belong to user"
      end
    end
  end

end
