class Goal 
  include MongoMapper::Document

  key :name, String, :required => true
  key :priority, Integer
  key :userid, Integer, :required => true
  key :is_complete, Boolean

  many :tasks
  # validates_presence_of :name   ### place inline instead, see above, key method
end
