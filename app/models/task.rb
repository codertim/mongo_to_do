class Task
  include MongoMapper::Document

  key :goal_id, ObjectId
  key :name, String
  key :is_finished, Boolean

  belongs_to :goal
end
