# Be sure to restart your server when you modify this file.

# Your secret key for verifying cookie session data integrity.
# If you change this key, all old sessions will become invalid!
# Make sure the secret is at least 30 characters and all random, 
# no regular words or you'll be exposed to dictionary attacks.
ActionController::Base.session = {
  :key         => '_mongotodo_session',
  :secret      => '660bd8038bd5ed341421f7f40f8a1ad0ddd20d7e4b9515b87f43e9205ec8fbab79972ef031f8f5e69bfba253ed2272925ed6c4d8a90acb356de30e74947c89bb'
}

# Use the database for sessions instead of the cookie-based default,
# which shouldn't be used to store highly confidential information
# (create the session table with "rake db:sessions:create")
# ActionController::Base.session_store = :active_record_store
